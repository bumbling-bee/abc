FROM node:8 as builder

# make our app directory
RUN mkdir /abc

# set working directory to our app directory
WORKDIR /abc

# Update before installing dependencies
RUN apt-get update
RUN apt-get -y upgrade

# Get pip for Flask
RUN apt-get -y install python3-pip
RUN pip3 install --upgrade pip

# Copy over pip requirements
COPY requirements.txt /abc

# Install the python server library
RUN pip3 install -r /abc/requirements.txt

# Copy over npm requirements
COPY package.json /abc
COPY yarn.lock /abc

# Install dependencies
RUN yarn install

# Copy app directory
COPY . /abc

# # Build the resources
RUN yarn build

# Set up environment variables
ENV FLASK_APP=/abc/run.py

# needed for flask to run with python3
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# RUN mkdir /abc/install
# RUN pip3 install --install-option="--prefix=/abc/install" -r /abc/requirements.txt

# Run the image as a non-root user
RUN useradd -m bumblingbee
USER bumblingbee

CMD python3 run.py


###########################

FROM python:3.5.6-alpine3.8

# make our app directory
RUN mkdir /abc
RUN mkdir /abc/app

# set working directory to our app directory
WORKDIR /abc

RUN apk update && \
  apk add postgresql-libs && \
  apk add --virtual .build-deps gcc musl-dev postgresql-dev

# Copy over pip requirements
COPY requirements.txt /abc

# Install the python server library
RUN pip3 install -r /abc/requirements.txt --no-cache-dir 

COPY --from=builder /abc/app/static /abc/app/static
COPY --from=builder /abc/app/templates /abc/app/templates
COPY --from=builder /abc/app/__init__.py /abc/app/__init__.py
COPY --from=builder /abc/app/routes.py /abc/app/routes.py
COPY --from=builder /abc/run.py /abc/run.py
COPY --from=builder /abc/app/assets /abc/app/static

# Set up environment variables
ENV FLASK_APP=/abc/run.py

# needed for flask to run with python3
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# Run the image as a non-root user
RUN adduser -S bumblingbee
USER bumblingbee

CMD python3 run.py