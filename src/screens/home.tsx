import * as React from 'react'
import * as ReactDOM from 'react-dom'
import 'bulma/bulma.sass'

import { HomeStyle, Navbar, Timeline, Wordcloud } from './home/index'
import * as WordCloudTypes from './home/wordcloud/types'
import * as WorkHistory from 'utils/workHistory'

const convertSkillsToWords = (skills: WorkHistory.Skill[]) =>
  skills.map(({ skill, proficiency, iconUrl }) => ({
    word: skill,
    weight: proficiency,
    iconUrl
  })) as WordCloudTypes.Word[]

const Home = () => {
  const [ skills, setSkills ] = React.useState([] as WorkHistory.Skill[][])
  const [ words, setWords ] = React.useState([] as WordCloudTypes.Word[])
  const [ workTimeline, setWorkTimeline ] = React.useState([] as WorkHistory.Job[])
  const wordcloudRef: React.RefObject<HTMLDivElement> = React.useRef()

  React.useEffect(() => {
    // set up initial work history and skills
    WorkHistory.getWorkHistory().then((workHistory: WorkHistory.WorkExperience[]) => {
      const tempTimeline: WorkHistory.Job[] = []
      const tempSkills: WorkHistory.Skill[][] = []
      workHistory.forEach(({ job, skills }) => {
        tempTimeline.push(job)
        tempSkills.push(skills)
      })
      setSkills(tempSkills)
      setWords(convertSkillsToWords(workHistory[0].skills))
      setWorkTimeline(tempTimeline)
    })
  }, [])

  const handlePositionUpdate = (coords: { x: number, y: number }[]) => {
    // not initialized yet
    if (!wordcloudRef || !wordcloudRef.current || !coords || !coords.length) return

    const wordcloudRect = wordcloudRef.current.getBoundingClientRect() as DOMRect
    console.log(wordcloudRect)
    const wordcloudCoord: {
      x: number
      y: number
    } = {
      x: wordcloudRect.x,
      y: wordcloudRect.y + (40 - wordcloudRect.y) / 2 + wordcloudRect.height / 2
    }

    let currentJob = 0
    let nextJob = 0
    const maxIndex = coords.length - 1
    coords.forEach((coord, index) => {
      if (coord.y < wordcloudCoord.y) {
        currentJob = index
        nextJob = Math.min(index + 1, maxIndex)
      }
    })

    const currentSkills = skills.reduce((skillAcc, skillsToAdd, jobIndex) => {
      if (jobIndex > currentJob) return skillAcc
      // deteriate old skills and adds new skills
      const skillMap = WorkHistory.addSkillsToMap(
          WorkHistory.decaySkillsAsMap(skillAcc), skillsToAdd
        )
      return WorkHistory.convertSkillMapToObject(skillMap)
    })

    // just return the current set if we don't need to scale between two jobs
    if (currentJob === nextJob) return convertSkillsToWords(currentSkills)

    // figure out distance between jobs
    const distPastFirst = wordcloudCoord.y - coords[currentJob].y
    const distToNext = coords[nextJob].y - wordcloudCoord.y
    const totalDist = distPastFirst + distToNext
    const progressTowardsNext = distPastFirst / totalDist

    const totalSkills = WorkHistory.addSkillsToMap(
      WorkHistory.decaySkillsAsMap(currentSkills, progressTowardsNext), skills[nextJob], progressTowardsNext
    )
    
    const newWords = convertSkillsToWords(WorkHistory.convertSkillMapToObject(totalSkills))
    console.log(newWords)

    setWords(newWords)
  }

  return (
    <>
      <Navbar key="navbar" title="alex chang"/>
      <section key="mainbody" className={`columns ${HomeStyle.body}`}>
        <div key="wordcloud" className={`column ${HomeStyle.wordCloudColumn}`} id="wordcloud-container">
          <Wordcloud 
            words={words}
            useIcons={false}
            ref={wordcloudRef}
          />
        </div>
        <div key="timeline" className="column" id="timeline-container">
          <Timeline
            position="left"
            timeline={workTimeline}
            updatePositions={handlePositionUpdate}
          />
        </div>
      </section>
    </>
  )
}
ReactDOM.render(<Home />, document.getElementById("app"))