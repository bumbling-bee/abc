export interface Word {
  word: string
  weight: number
  iconUrl?: string
}