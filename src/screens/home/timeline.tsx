import * as React from 'react'

import { VerticalTimeline }  from 'react-vertical-timeline-component'
import { Job } from 'utils/workHistory'
import TimelineElement from './timeline/timelineElement'

import 'react-vertical-timeline-component/style.min.css'
import * as TimelineStyle from './timeline/assets/timeline.scss'

interface TimelineProps {
  position?: 'left' | 'right'
  timeline: Job[]
  updatePositions: (coords: {
    x: number,
    y: number,
  }[]) => void
}

const ArrowOffset = 16;
const ArrowHeight = 14;

const Timeline = ({
  position,
  timeline,
  updatePositions
}: TimelineProps) => {
  const [ timelineRefs, setTimelineRef ] = React.useState([] as React.RefObject<Element>[])
  React.useEffect(() => {
    const updateNewPositions = () => {
      const coords = []
      timelineRefs.forEach((timelineRef) => {
        if (timelineRef.current) {
          const rect = timelineRef.current.getBoundingClientRect() as DOMRect
          coords.push({
            x: rect.x + rect.width,
            y: rect.y + ArrowOffset + ArrowHeight / 2
          })
        }
      })
      updatePositions(coords)
    }
    window.addEventListener('scroll', updateNewPositions)
    updateNewPositions()
    return () => {
      window.removeEventListener('scroll', updateNewPositions)
    }
  }, [timelineRefs])

  const refs = []
  const timelineComponent = React.useMemo(() => timeline.map((job: Job, index) => {
    refs.push(React.useRef())
    return (
      <TimelineElement
        key={index}
        ref={refs[index]}
        {...job}
      />
  )}), [timeline])

  // set new refs if the length is different
  if (refs.length !== timelineRefs.length) {
    setTimelineRef(refs)
  }

  return (
    <div className={position === 'left' ? TimelineStyle.left : ''}>
      <VerticalTimeline
        key="vertTimeline"
        layout="1-column"
      >
        {timelineComponent}
      </VerticalTimeline>
      <div key="placeholder" style={{ height: '512px'}}/>
    </div>
  )
}

export default Timeline