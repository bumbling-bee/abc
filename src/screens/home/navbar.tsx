import * as React from 'react'

import * as navbarStyle from './navbar/assets/navbar.scss'

interface navbarProps {
  title: string
}

interface navbarState {
  titleRef?: any
}

class Navbar extends React.Component<navbarProps, navbarState> {
  titleRef: any

  constructor(props: navbarProps) {
    super(props)
    this.titleRef = React.createRef()

    this.state = {
      titleRef: null
    }
  }

  componentDidMount() {
    this.setState({ titleRef: this.titleRef })
  }

  render() {
    const { title } = this.props
    const { titleRef } = this.state

    const left = titleRef ? titleRef.current.offsetLeft : 0
    const width = titleRef ? titleRef.current.clientWidth : 0
    const underlineStyle: React.CSSProperties = {
      left: `${left}px`,
      width: `${width}px`
    }

    return (<nav className={`navbar navbar-container ${navbarStyle.container}`}>
      <div className='navbar-brand'>
        <h1
          className={`is-size-2 ${navbarStyle.title}`}
          ref={this.titleRef}
        >
          {title}
        </h1>
        <hr
          className={navbarStyle.underline}
          style={underlineStyle}
        />
      </div>
    </nav>)
  }
}

export default Navbar