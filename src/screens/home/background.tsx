import * as React from 'react'
import * as style from './background/assets/background.scss'

interface backgroundProps { 
  children: React.ReactNode
}

const Background = ({
  children
}: backgroundProps) => (
  <div className={style.background}>
    {children}
  </div>
)

export default Background