import * as React from 'react'
import WordCloud from 'react-tag-cloud'
// import ScaledWordCloud from '../../lib/scaledTagCloud'

import * as WordCloudType from './wordcloud/types'
import * as WordCloudStyle from './wordcloud/assets/wordcloud.scss'

// mini hack to have icons for word cloud stuff
// fontSize: 60,
// fontWeight: 'bold',
// color: 'transparent',
// backgroundImage: 'url(https://bulma.io/images/bulma-logo.png)',
// backgroundSize: 'cover'


interface WordcloudProps {
  words: WordCloudType.Word[],
  useIcons: boolean,
}

const Wordcloud = ({
  words,
  useIcons,
} : WordcloudProps,
ref: React.RefObject<HTMLDivElement>) => {
  const wordcloudRef: React.RefObject<HTMLDivElement> = React.useRef()
  const [ innerWidth, setInnerWidth ] = React.useState(0)
  React.useImperativeMethods(ref, () => ({
    getBoundingClientRect: () => {
      const domNode = wordcloudRef.current
      return domNode.getBoundingClientRect()
    }
  }))

  React.useEffect(() => {
    console.log((wordcloudRef.current as Element).getBoundingClientRect())
    const updateInner = () => {
      if (innerWidth !== window.innerWidth) {
        setInnerWidth(innerWidth)
      }
    }
    window.addEventListener("resize", updateInner)
    updateInner()
    return () => {
      window.removeEventListener("resize", updateInner)
    }
  }, [])

  const getTextWidth = (txt, fontSize) =>
    txt.length * fontSize
  

  const normalize = (
    words: WordCloudType.Word[],
    width: number): WordCloudType.Word[] => {
      if (!words || !words.length) return words
      const wordSize = ({ word, weight }:
        { word: string, weight: number }) =>
          getTextWidth(word, weight)
      // normalize largest word to take up 95% of the width
      const heaviest = words.reduce((largestWord, currentWord) => 
        wordSize(largestWord) > wordSize(currentWord) ?
          largestWord : currentWord
      )
      const normalizationFactor = width * .95 / wordSize(heaviest)

      const normalizedWords = words.map((word: WordCloudType.Word) =>
        ({
          ...word,
          weight: word.weight * normalizationFactor
        })
      )
      return normalizedWords
  }

  const rect = wordcloudRef && wordcloudRef.current ? (wordcloudRef.current as Element).getBoundingClientRect() : null
  const width = rect ? rect.width : null
  const height = rect ? rect.height : null
  
  const wordcloud = wordcloudRef && words ? (<WordCloud 
    className={WordCloudStyle.tagCloud}
    style={{
          fontFamily: '"arcon round", sans-serif',
          fontSize: 10,
          width: 'auto',
          padding: 10,
          height: `calc(${height}px - 20px)`,
        }}
        random={() => .5}>
        {normalize(words, width).map(({word, weight}, index) => (
          <div key={index} style={{fontSize: weight}}>{word}</div>
        ))}
        </WordCloud>) : null

  return (
    <div className={WordCloudStyle.cloud} ref={wordcloudRef as React.RefObject<HTMLDivElement>}>
      {wordcloud}
    </div>
    )
}

export default React.forwardRef(Wordcloud)