import * as React from 'react'

const defaultStyle: React.CSSProperties = {
  height: '100%',
  width: '100%',
  borderRadius: '100%',
}

// individual overrides
const divStyles = {
  'https://www.android.com/static/img/favicon.ico': {
    background: 'white',
  },
  'https://www.coursehero.com/chfavicon.ico': {
    padding: '9px 7px 5px 7px',
    background: 'white',
  }
}

const imgStyles = {
  'https://www.android.com/static/img/favicon.ico': {
    padding: '5px'
  },
  'https://www.coursehero.com/chfavicon.ico': {
    borderRadius: '0',
  }
}

interface IconProp {
  url: string,
  style?: string,
}

const Icon = ({ url } : IconProp) => (
  <div style={{
      ...defaultStyle,
      ...(divStyles[url] || {})
    }}>
    <img src={url} style={{
        ...defaultStyle,
        ...(imgStyles[url] || {})
      }}></img>
  </div>
)

export default Icon