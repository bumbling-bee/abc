import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { VerticalTimelineElement }  from 'react-vertical-timeline-component'

import Icon from './timelineElement/icon'

interface TimelineElementProps {
  date: string
  icon: string
  position: string
  companyUrl: string
  company: string
  description: string[]
}

const TimelineElement = ({
  date,
  icon,
  position,
  companyUrl,
  company,
  description
} : TimelineElementProps,
ref: React.RefObject<VerticalTimelineElement>) => {
  let descriptionComp = []
  description.forEach((desc, index) => {
    descriptionComp = 0 === descriptionComp.length ? [ desc ] : [ ...descriptionComp, (<br key={index} />), desc ]
  })
  const elementRef: React.RefObject<VerticalTimelineElement> = React.useRef()
  React.useImperativeMethods(ref, () => ({
    getBoundingClientRect: () => {
      const domNode = ReactDOM.findDOMNode(elementRef.current) as Element
      return domNode.getBoundingClientRect()
    }
  }))

  return (<VerticalTimelineElement
      className="vertical-timeline-element--work"
      ref={elementRef}
      date={date}
      icon={<Icon url={icon}/>}
      iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
    >
      <h3 className="vertical-timeline-element-title">{position}</h3>
      <h4 className="vertical-timeline-element-subtitle">
        <a href={companyUrl}>{company}</a>
      </h4>
      <p>
      {descriptionComp}
      </p>
    </VerticalTimelineElement>)
}

export default React.forwardRef(TimelineElement)