declare module '*.sass' {
  const content: {[className: string]: string};
  export = content;
}

declare module '*.scss' {
  const content: {[className: string]: string};
  export = content;
}

declare module "*.jpg" {
  const content: any;
  export default content;
}