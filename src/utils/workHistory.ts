export interface Job {
  icon: string
  position: string
  company: string
  companyUrl: string
  date: string
  description: string[]
}

export interface Skill {
  skill: string
  proficiency: number
  iconUrl?: string
}

export interface WorkExperience {
  job: Job
  skills: Skill[]
}

export const SkillDecayRate = 20
export const MinSkillProficiency = 10

export interface SkillMap {
  [skill: string]: {
    proficiency: number
    iconUrl?: string
  }
}

export const decaySkillsAsMap = (skills: Skill[], decayPercent: number = 1) => {
  const skillMap: SkillMap = {}
  skills.forEach(({ skill, proficiency, iconUrl }) => {
    skillMap[skill] = {
      // min proficiency is 10 and deteriates at a rate of 20 per job
      proficiency: Math.max(
        proficiency - SkillDecayRate * decayPercent,
        MinSkillProficiency
      ),
      iconUrl
    }
  })

  return skillMap
}

export const addSkillsToMap = (
  oldSkills: SkillMap,
  newSkills: Skill[],
  percentProficient: number = 1) => {
    newSkills.forEach(({ skill, proficiency, iconUrl}) => {
      const carryOver = oldSkills[skill] ? oldSkills[skill].proficiency : 0
      oldSkills[skill] = {
        proficiency: proficiency * percentProficient + carryOver,
        iconUrl
      }
    })

    return oldSkills
  }

export const convertSkillMapToObject = (skillMap: SkillMap) => {
  const finalSkills: Skill[] = []
  for (const skill in skillMap) {
    finalSkills.push({
      skill,
      ...skillMap[skill],
    })
  }

  return finalSkills
}

// Note: skills deteriate at a rate of 20 per work experience until a minimum of 10
export const getWorkHistory: () => Promise<WorkExperience[]> = async () => Promise.resolve([
  // UCLA
  {
    job: {
      icon: 'http://www.ucla.edu/img/favicon.ico', // http://www.ucla.edu/img/favicon.ico
      position: 'Student',
      company: 'UCLA',
      companyUrl: 'http://www.ucla.edu/',
      date: '2008 - 2012',
      description: [
        'Graduated with a B.S. Computer Science and Engineering',
        'Active member of the kendo club',
      ]
    },
    skills: [
      {
        skill: 'C++',
        proficiency: 100,
      },
      {
        skill: 'OOP',
        proficiency: 100,
      },
      {
        skill: 'Algorithms',
        proficiency: 100,
      },
      {
        skill: 'Kendo',
        proficiency: 50,
      },
    ]
  },
  // Zugara
  {
    job: {
      icon: 'https://zugara.com/wp-content/uploads/fbrfg/favicon.ico', // https://zugara.com/wp-content/uploads/fbrfg/favicon.ico
      position: 'Intern',
      company: 'Zugara',
      companyUrl: 'https://zugara.com/',
      date: '2012 (7 months)',
      description: [
        'Created PoC integration with a new depth sensing camera to further enhance augmented reality interactions',
      ]
    },
    skills: [
      {
        skill: 'C',
        proficiency: 70,
      },
      {
        skill: 'Augmented Reality',
        proficiency: 70,
      },
    ]
  },
  // Tokyo Electron
  {
    job: {
      icon: 'https://www.tel.com/favicon.ico', // https://www.tel.com/favicon.ico
      position: 'Software Engineer I',
      company: 'Tokyo Electron',
      companyUrl: 'https://www.tel.com/',
      date: '2012 - 2013',
      description: [
        `Worked with team on getting product ready for release by cleaning up bugs and creating a consistent visual design`,
      ]
    },
    skills: [
      {
        skill: 'C#',
        proficiency: 70,
      },
      {
        skill: 'WPF',
        proficiency: 70,
      },
      {
        skill: 'Debugging',
        proficiency: 70,
      },
    ]
  },
  {
    job: {
      icon: 'https://www.tel.com/favicon.ico', // https://www.tel.com/favicon.ico
      position: 'Software Engineer II',
      company: 'Tokyo Electron',
      companyUrl: 'https://www.tel.com/',
      date: '2013 - 2015',
      description: [
        'Lead effort in converting a Matlab PoC into a full production ready WPF application',
        'Worked with team in order to refactor existing application into a client / server model to improve reliability',
      ]
    },
    skills: [
      {
        skill: 'C#',
        proficiency: 50,
      },
      {
        skill: 'WPF',
        proficiency: 50,
      },
      {
        skill: 'MVVM',
        proficiency: 50,
      },
      {
        skill: 'Debugging',
        proficiency: 30,
      }
    ]
  },
  // Android
  {
    job: {
      icon: 'https://www.android.com/static/img/favicon.ico', // https://www.android.com/static/img/favicon.ico
      position: 'Android Developer in Training',
      company: 'Alphabet Soup Programming',
      companyUrl: 'https://www.android.com/',
      date: '2015 (4 months)',
      description: [
        'Took time off to explore Android development',
        'Successfully created an alpha build android app that interacted with an AWS MySql instance to share data between users',
      ]
    },
    skills: [
      {
        skill: 'AWS',
        proficiency: 50,
      },
      {
        skill: 'MySql',
        proficiency: 30,
      },
      {
        skill: 'Java',
        proficiency: 50,
      },
      {
        skill: 'Debugging',
        proficiency: 20,
      }
    ]
  },
  // Course Hero
  {
    job: {
      icon: 'https://www.coursehero.com/chfavicon.ico', // https://www.coursehero.com/chfavicon.ico
      position: 'Software Engineer I',
      company: 'Course Hero',
      companyUrl: 'https://www.coursehero.com/',
      date: '2016 - 2017',
      description: [
        `Maintained and investigated the inner workings of business critical (payments, compliance, etc.)
        legacy systems in an effort to have the team be the product and knowledge owner`,
      ]
    },
    skills: [
      {
        skill: 'AWS',
        proficiency: 30,
      },
      {
        skill: 'MySql',
        proficiency: 50,
      },
      {
        skill: 'PHP',
        proficiency: 100,
      },
      {
        skill: 'Debugging',
        proficiency: 30,
      },
      {
        skill: 'Docker',
        proficiency: 30,
      }
    ]
  },
  {
    job: {
      icon: 'https://www.coursehero.com/chfavicon.ico', // https://www.coursehero.com/chfavicon.ico
      position: 'Software Engineer II',
      company: 'Course Hero',
      companyUrl: 'https://www.coursehero.com/',
      date: '2017 - 2018',
      description: [
        'Architected and lead 3 month long team initiative in refactoring our legacy payments system into a microservice',
        'Started the use of feature flags in order to easily test the new microservice in production as well as do a gradual rollout',
      ]
    },
    skills: [
      {
        skill: 'AWS',
        proficiency: 50,
      },
      {
        skill: 'MySql',
        proficiency: 40,
      },
      {
        skill: 'PHP',
        proficiency: 20,
      },
      {
        skill: 'Debugging',
        proficiency: 30,
      },
      {
        skill: 'Docker',
        proficiency: 50,
      },
    ]
  },
  {
    job: {
      icon: 'https://www.coursehero.com/chfavicon.ico', // https://www.coursehero.com/chfavicon.ico
      position: 'Senior Software Engineer',
      company: 'Course Hero',
      companyUrl: 'https://www.coursehero.com/',
      date: '2018 - present',
      description: [
        'Helped in the launch of two new microservices that were necessary to get a new product into production',
        'Focused on goal oriented initiatives which helped lead us to the team\'s first quarter hitting all three major goals',
        'Designed and trialed a software engineer coaching initiative in order to help provide additional guidance to more junior engineers',
      ]
    },
    skills: [
      {
        skill: 'AWS',
        proficiency: 20,
      },
      {
        skill: 'MySql',
        proficiency: 20,
      },
      {
        skill: 'PHP',
        proficiency: 20,
      },
      {
        skill: 'Debugging',
        proficiency: 20,
      },
      {
        skill: 'Docker',
        proficiency: 20,
      },
      {
        skill: 'React',
        proficiency: 80,
      },
    ]
  },
])